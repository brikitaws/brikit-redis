{
  "service-name": {
    "comment": "the name for this service matches the version of redis being run",
    "value": "yac/redis:6.3.17"
  }, 
  "service-description": {
    "summary": "Redis service running in a 2-subnet-layer VPC",
    "details":  ["* HA though ASG and ECS",
                 "* Backup/Recovery of home via S3"],
    "stack-diagram"  : "http://imgh.us/yac_redis_stack.png",
    "maintainer" : {
      "name":     "Thomas Jackson",
      "email":    "thomas.b.jackson@gmail.com"
    }
  },
  "files-for-boot": {
    "comment": "files to deploy before starting this service",
    "value": [
      {
        "src": "config/backups.json",
        "dest": {"yac-join" : [ "/", [
                   "s3:/", 
                  {"yac-ref": "s3-bucket"},
                  {"yac-ref": "app"},
                  {"yac-ref": "suffix"},
                  {"yac-ref": "env"},
                   "backups.json" ]]}
      },
      {
        "src":  "config/cwlogs.conf",
        "dest": {"yac-join" : [ "/", [ 
                   "s3:/",
                  {"yac-ref": "s3-bucket"},
                  {"yac-ref": "app"},
                  {"yac-ref": "suffix"},
                  {"yac-ref": "env"},
                   "cwlogs.conf" ]]
                }
      }
    ]
  },  
  "service-params": {
    "comment": "constants for this service. any can be rendered into templates (cf or ecs) via yac-ref",

    "availability-zones": {
      "comment": "build in Oregon",
      "value": [ "us-west-2a", "us-west-2b" ]  
    },
    "exclusions": {
      "comment": ["per the diagram, exclude the internal elb, an rds instance, and efs"],
      "value": ["i-elb", "rds", "efs"]
    },     
    "stack-description": {
      "comment": "for describing the stack in the cloud formation UI",
      "value": "Redis service running in a VPC with 2 subnet layers"
    },     
    "service-port": {
      "comment": "the port redis should listen on",
      "value": 6379
    }, 
    "service-port-str": {
      "comment": "the port redis should listen on (as a string)",
      "value": "6379"
    },  
    "cost-center": {
      "comment": "used to tag resources so resource costs can be rolled up per cost center",
      "value":   "747"
    },   
    "owner": {
      "comment": "used to tag the owner of all yac-related resources in the brikit-internal vpc",
      "value": "darryl@brikit.com"
    },     
    "iam-role": {
      "comment":  "iam role to use with the redis service",
      "value":  "brikit-yac-user"
    },
    "ssh-key": {
      "comment":  "ssl key pair for ssh access to the EC2 instance",
      "value":  "yac-internal-key"
    },
    "ssl-cert": {
      "comment": "the wildcard cert for all apps in the brikit.com domain",
      "value": ".brikit.com"
    },
    "s3-bucket": {
      "comment": "s3 bucket that my apps will use for config files, backups, etc.",
      "value": "brikit-internal-yac-apps"
    },
    "boot-file": {
      "comment": "the file that the the ec2 instance should use to boot from",
      "value":   "config/boot.sh"
    }     
  },
  "service-inputs": {
    "comments": ["script that will be called to prompt the user for vpc-related params, and to either ",
                 "accept the default service-params, or provide new params at service installation or update time"],
    "value":  "lib/inputs.py"  
  },  
  "CloudFormation": {      
    "Parameters" : {
      "ImageId" : {
        "Description" : "CoreOS-stable-835.9.0-hvm, December 6, 2015",
        "Type" :        "String",
        "Default" :     "ami-16cfd277"
      },    
      "InstanceType" : {
        "Description" : "Type of EC2 instance to launch. Set to be as small as possible.",
        "Type" :        "String",
        "Default" :    "t2.small"
      },
      "DockerVolumeSize" : {
        "Description" : "Only have a few containers so only a few container images to store",
        "Type" : "String",
        "Default" : "10"
      }, 
      "HomeVolumeSize" : {
        "Description" : "Size to support the intended size of the service DB (small in near future)",
        "Type" : "String",
        "Default" : "10"
      }      
    },
    "Resources": {
      "ECS": {
        "Type": "AWS::ECS::Cluster"
      },
      "RedisService": {
        "Type" : "AWS::ECS::Service",
        "Properties" : {
          "Cluster" : { "Ref": "ECS" },
          "DesiredCount" : 1,
          "TaskDefinition" : { "Ref" : "RedisTD" }
        }
      },
      "RedisTD": {
        "Type" : "AWS::ECS::TaskDefinition",
        "Properties" : {
          "ContainerDefinitions": [
            {
              "Name": "redis", 
              "Image": "redis:latest",
              "Command": ["/entrypoint.sh","redis-server","--appendonly","yes"],
              "Cpu": 0,
              "Essential": true,
              "VolumesFrom": [],
              "Memory": 512,               
              "PortMappings": [
                {
                  "HostPort": {"yac-ref": "service-port"},
                  "ContainerPort": 6379
                }
              ],
              "MountPoints": [
                {
                  "ContainerPath": "/data",
                  "SourceVolume": "redis-data",
                  "ReadOnly": false
                },
                {
                  "ContainerPath": "/var/log/redis",
                  "SourceVolume": "redis-logs",
                  "ReadOnly": false
                },
                {
                  "ContainerPath": "/etc/localtime",
                  "SourceVolume": "localtime",
                  "ReadOnly": true
                }
              ]
            },
            {
              "Name": "backups",
              "Image": "nordstromsets/backups:latest",
              "Cpu": 0,
              "VolumesFrom": [],
              "Memory": 512,
              "PortMappings": [],
              "Essential": false,
              "MountPoints": [
                {
                  "ContainerPath": "/var/local/redis/data",
                  "SourceVolume": "redis-data",
                  "ReadOnly": false
                },
                {
                  "ContainerPath": "/var/local/backups",
                  "SourceVolume": "backup-logs",
                  "ReadOnly": false
                }
              ],
              "Environment": [
                {
                  "Name": "HOSTNAME",
                  "Value": {"yac-ref": "host-name"}
                },
                {
                  "Name": "CONFIGS_PATH",
                  "Value": {"yac-join" : [ "/", [ 
                     "s3:/",
                    {"yac-ref": "s3-bucket"},
                    {"yac-ref": "app"},
                    {"yac-ref": "suffix"},
                    {"yac-ref": "env"},
                    "backups.json" ]]}
                }
              ]
            }            
          ],
          "Volumes": [
            {
              "Host": {
                "SourcePath": "/var/local/redis/data"
              },
              "Name": "redis-data"
            },
            {
              "Host": {
                "SourcePath": "/var/log/redis"
              },
              "Name": "redis-logs"
            },      
            {
              "Host": {
                "SourcePath": "/etc/localtime"
              },
              "Name": "localtime"
            },
            {
              "Host": {
                "SourcePath": "/var/local/redis/backup-logs"
              },
              "Name": "backup-logs"
            }
          ]
        }
      }
    },
    "ResourceTweaks": {
      "AppLaunchConfig" : {
        "Type" : "AWS::AutoScaling::LaunchConfiguration",
        "Properties" : {
          "AssociatePublicIpAddress": true
        }
      },
      "AppSG" : {
        "Type" : "AWS::EC2::SecurityGroup",
        "Properties" : {
          "SecurityGroupIngress" : [
            {
              "IpProtocol" : "tcp",
              "FromPort" : "22",
              "ToPort" : "22",
              "CidrIp" : "0.0.0.0/0"
            }
          ]
        }
      },
      "ExternalElbSG" : {
        "Type" : "AWS::EC2::SecurityGroup",
        "Properties" : {
          "GroupDescription" : "Enable inbound TCP access on the configured redis port",
          "SecurityGroupIngress" : [ 
            {
              "IpProtocol" : "tcp",
              "FromPort" : {"yac-ref": "service-port-str"},
              "ToPort" : {"yac-ref": "service-port-str"},
              "CidrIp" : "0.0.0.0/0"
            }
          ]
        }
      },
      "ExternalElbHttpIngress" : {
        "Type" : "AWS::EC2::SecurityGroupIngress",
        "Properties" : {
          "GroupId" : { "Fn::GetAtt": ["AppSG","GroupId"]},
          "IpProtocol" : "tcp",
          "FromPort" : {"yac-ref": "service-port-str"},
          "ToPort" : {"yac-ref": "service-port-str"},
          "SourceSecurityGroupId" : { "Fn::GetAtt": ["ExternalElbSG","GroupId"] }
        }
      },
      "ExternalElbHttpEgress" : {
        "Type" : "AWS::EC2::SecurityGroupEgress",
        "Properties" : {
          "GroupId" : { "Fn::GetAtt": ["ExternalElbSG","GroupId"]},
          "IpProtocol" : "tcp",
          "FromPort" : {"yac-ref": "service-port-str"},
          "ToPort" : {"yac-ref": "service-port-str"},
          "DestinationSecurityGroupId" : {"Fn::GetAtt": ["AppSG","GroupId"]}
        }
      },
      "ExternalElb" : {
        "Type" : "AWS::ElasticLoadBalancing::LoadBalancer",
        "Properties" : {
          "Listeners" : [ 
            {
              "LoadBalancerPort" : {"yac-ref": "service-port-str"},
              "InstancePort" : {"yac-ref": "service-port-str"},
              "InstanceProtocol": "TCP",
              "Protocol" : "TCP"
            }
          ],
          "HealthCheck" : {
            "Target" : {"yac-join" : [ ":", [ "TCP", {"yac-ref": "service-port-str"}]]},
            "HealthyThreshold" : "3",
            "UnhealthyThreshold" : "5",
            "Interval" : "20",
            "Timeout" : "5"
          }
        }     
      }
    }
  }
}