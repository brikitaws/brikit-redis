#!/usr/bin/env python

import boto3, json, jmespath
from yac.lib.naming import get_stack_name
from yac.lib.variables import get_variable, set_variable
from yac.lib.stack import stack_exists
from botocore.exceptions import ClientError

VPC_MAPPING = {
    "comment": "the default vpc for each redis environment",
    "dev":     ["internal"],
    "stage":   ["internal"],
    "prod":    ["internal"]
  }

# possible service states
CREATING = "CREATING"
UPDATING = "UPDATING"

service_params_needed = {
    "ssh-key": {
      "comment":  "the key to use for ssh access",
    }, 
    "vpc-id": {
      "comment": "the id (or friendly name) of the vpc where this stack should be built",
    },
    "private-subnet-ids": {
      "comment":  "the id(s) of the subnet(s) in your private subnet layer (see diagram)",
    },
    "dmz-subnet-ids": {
      "comment":  "the id(s) of the subnet(s) in your dmz subnet layer (see diagram)",
    },    
    "public-subnet-ids": {
      "comment":  "the id(s) of the subnet(s) in your public subnet layer (see diagram)",
    },
    "restore-config": {
        "comment": "same url as for the backups configs - restoring is the inverse of backup",
        "value": {"yac-join" : [ "/", [ 
          {"yac-ref": "s3-bucket"},
          {"yac-ref": "app"},
          {"yac-ref": "suffix"},
          {"yac-ref": "env"},
           "backups.json" ]]}
    }
} 

# populate vpc-related params for the jira service
def get_value(params):

    input_params = {}

    vpc = get_vpc(params)

    # get availability zones from params
    availabilty_zones = get_variable(params,"availability-zones",[])

    # get subnets for this vpc
    private_subnets = get_subnets(vpc, availabilty_zones,"internal-private")
    public_subnets = get_subnets(vpc, availabilty_zones,"internal-public")

    # dmz and private subnets are one and the same
    dmz_subnet_ids = jmespath.search('Subnets[*].SubnetId',private_subnets)
    private_subnets_ids = jmespath.search('Subnets[*].SubnetId',private_subnets)
    public_subnets_ids = jmespath.search('Subnets[*].SubnetId',public_subnets)

    if vpc:
        set_variable(input_params,'vpc-id',vpc["VpcId"], "vpc id")
    if dmz_subnet_ids:
        set_variable(input_params,'dmz-subnet-ids',dmz_subnet_ids, "subnets ids for dmz subnets")
    if private_subnets_ids:
        set_variable(input_params,'private-subnet-ids',private_subnets_ids, "subnets ids for dmz subnets") 
    if public_subnets_ids:
        set_variable(input_params,'public-subnet-ids',public_subnets_ids, "subnets ids for public subnets")        

    set_variable(input_params,
                 'host-name', get_host_name(params), 
                 "host name for ec2 instance hosting redis")

    set_variable(input_params,
                 'restore-configs', get_restore_configs_url(params), 
                 "configs for the backups container to restore from on boot")    

    set_variable(input_params,
                 's3-path', get_s3_path(params), 
                 "path to location in s3 to save configs")

    return input_params           


# get the vpc to use for a given environment
def get_vpc(params):

    vpc = {}

    ec2 = boto3.client('ec2')

    # get the env
    env  = get_variable(params,'env')

    # get the status of the stack
    stack_name = get_stack_name(params)

    if not stack_exists(stack_name):

        # use the default vpc for this env, per VPC_MAPPING

        # Environment tag contains VPC_MAPPING values
        vpcs = ec2.describe_vpcs(Filters=[
                {
                    'Name': 'tag-key',
                    'Values': [
                        'Name',
                    ]
                },
                {
                    'Name': 'tag-value',
                    'Values': VPC_MAPPING[env],
                }])

        if 'Vpcs' in vpcs and len(vpcs['Vpcs'])==1:
            vpc =  vpcs['Vpcs'][0]

    else:

        # determine the existing VPC in use for this service
        vpc = get_stack_vpc(params)

    return vpc

# get the subnets to use for a given vpc and a given set of 
# availability zones
def get_subnets(vpc, availabilty_zones, network):

    ec2 = boto3.client('ec2')

    subnets = []
    
    if vpc and 'VpcId' in vpc:

        subnets = ec2.describe_subnets(Filters=[
            {
                'Name': 'vpc-id',
                'Values': [
                    vpc['VpcId'],
                ]
            },
            {
                'Name': 'availability-zone',
                'Values': availabilty_zones,
            },
            {
                'Name': 'tag-key',
                'Values': [
                    'Name',
                ]
            },
            {
                'Name': 'tag-value',
                'Values': [
                    '%s*'%network
                ]
            }])

    else:
        print "vpc not present"

    return subnets

# get vpc associated with an existing stack
def get_stack_vpc( params ):

    vpc = {}

    # get the stack name 
    stack_name = get_stack_name( params )

    if stack_name:

        # get the stack
        cloudformation = boto3.client('cloudformation')

        try:
            stack = cloudformation.describe_stacks(StackName=stack_name)

            # get the first stack's 'stack id'
            stack_id = stack['Stacks'][0]['StackId']

            # filters for stack_id in cloudformation tag
            filters = [{'Name':"tag:aws:cloudformation:stack-id", 'Values' : [stack_id]}]

            #pull response with instances matching filters
            client = boto3.client('ec2')

            reservations = client.describe_instances(Filters=filters)

            intances = jmespath.search('Reservations[*].Instances',reservations)

            if len(intances)>=1:

                # use the vpd id of the first instance
                # print intances[0][0]
                vpc_id = intances[0][0]['VpcId']

                vpcs = client.describe_vpcs(VpcIds=[vpc_id])

                if 'Vpcs' in vpcs and len(vpcs['Vpcs'])==1:
                    vpc =  vpcs['Vpcs'][0]
        except ClientError as e:
            print "existing stack not found"

        return vpc

def get_host_name( params ):   

    name_parts = [get_variable(params,'app',''),
                  get_variable(params,'suffix',''), 
                  get_variable(params,'env','') ]

    # get rid of empty strings
    name_parts = filter(None,name_parts)

    host_name = '-'.join(name_parts)

    return host_name

def get_restore_configs_url( params ):   

    s3_path = get_s3_path( params )

    name_parts = [s3_path,
                  "backups.json"]

    restore_configs_url = '/'.join(name_parts)

    return restore_configs_url  

def get_s3_path( params ):   

    name_parts = ["s3://",
                  get_variable(params,'s3-bucket',''),
                  get_variable(params,'app',''),
                  get_variable(params,'suffix',''), 
                  get_variable(params,'env','')]

    # get rid of empty strings
    name_parts = filter(None,name_parts)

    s3_path = '/'.join(name_parts)

    return s3_path 