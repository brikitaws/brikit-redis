#!/bin/bash

# *****************************************************************
# For booting a new redis host.  
# Note that this stuff ONLY gets run at instance creation time.    
# *****************************************************************              

# {{parms}} get rendered in via yac/lib/boot.py

# ntp-servers are defined in vpc defs
NTP_SERVERS={{ntp-servers}}

# theres are auto-defined for all yac services based on cli inputs
HOST_NAME={{host-name}}

# restore-config and mount-dir are defined in the service def for redis
RESTORE_CONFIG={{restore-configs}}

# cluster name gets auto-injected once cluster gets created as part of 
# yac.lib.boot.get_value()

# format volumes as an ext4 fs
mkfs -t ext4 /dev/xvdc
mkfs -t ext4 /dev/xvdd

# create the app home and docker lib directories
# each will become the mount points on the volumes above.
mkdir -p /var/local/redis
mkdir -p /var/lib/docker

# mount the volumes
mount /dev/xvdc /var/lib/docker
mount /dev/xvdd /var/local/redis

# create the host volumes referenced in task-definitions/volumes
# (except /etc/localtime which comes baked into CoreOS AMI)
# give r/w/x(traversal) to everyone
mkdir /var/local/redis/data
mkdir -p /var/log/redis
mkdir -p /var/local/redis/backup-logs
chmod 777 -R /var/local/redis/data
chmod 777 -R /var/log/redis
chmod 777 -R /var/local/redis/backup-logs

# configure NTP
if [ $NTP_SERVERS ]; then
	# break existing symbolic link to shared conf file 
	rm /etc/ntp.conf
	# write the servers into a new conf file. servers are in a semicolon delimitted string. 
	# sed 'em into a conf file with one line per server
	echo $NTP_SERVERS | sed -e 's/;/\n/g' | sed -e 's/^/server /' > /etc/ntp.conf
	chmod 0666  /etc/ntp.conf
	# restart the ntp daemon so it picks up the new configs
	systemctl restart ntpd
fi

# set timezone
timedatectl set-timezone America/Los_Angeles

# create the docker-tcp.socket file, which enables docker's remote API on coreos
echo '[Unit]
	Description=Docker Socket for the API

	[Socket]
	ListenStream=5555
	BindIPv6Only=both
	Service=docker.service

	[Install]
	WantedBy=sockets.target' > /etc/systemd/system/docker-tcp.socket
chmod 0644      /etc/systemd/system/docker-tcp.socket

# reload and restart docker to pick up with the new proxy and docker configurations
systemctl enable docker-tcp.socket
systemctl stop docker
systemctl daemon-reload
systemctl start docker-tcp.socket
systemctl start docker

# restart update-engine to pick up any proxy configs
systemctl restart update-engine

if [ $RESTORE_CONFIG ] && [ $HOST_NAME ]; then
	# restore any home directory files that this instance should have at startup
	docker run --name restore-at-boot \
	--detach=true \
	--volume=/var/local/atlassian/jira:/var/local/atlassian/jira:rw \
	--volume=/var/local/atlassian/jira/backup-logs:/var/local/backups:rw \
	nordstromsets/backups:latest \
	python -m src.restore $RESTORE_CONFIG $HOST_NAME
fi

if [ $CLUSTER_NAME ]; then

	# install ECS agent and register this instance with the CLUSTER_NAME cluster
	docker run --name ecs-agent \
	--detach=true \
	--restart=on-failure:10 \
	--volume=/var/run/docker.sock:/var/run/docker.sock \
	--volume=/var/log/ecs/:/log \
	--volume=/var/lib/ecs/data:/data \
	--volume=/sys/fs/cgroup:/sys/fs/cgroup:ro \
	--volume=/var/run/docker/execdriver/native:/var/lib/docker/execdriver/native:ro \
	--publish=127.0.0.1:51678:51678 \
	--env=ECS_LOGFILE=/log/ecs-agent.log \
	--env=ECS_LOGLEVEL=info \
	--env=ECS_DATADIR=/data \
	--env=ECS_CLUSTER=$CLUSTER_NAME \
	amazon/amazon-ecs-agent:latest
fi
